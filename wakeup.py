#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import time
import math
from rpi_ws281x import Color
from grove.grove_ws2813_rgb_led_strip import GroveWS2813RgbStrip
from datetime import datetime

# connect to pin 12(slot PWM)
PIN   = 12
LED_COUNT = 15
strip = GroveWS2813RgbStrip(PIN, LED_COUNT)

def lightLEDs(lastLEDIsLit, litCount):
    onColour = Color(4,5,4)
    offColour = Color(0,0,0)
    for i in range(strip.numPixels() - 1):
        if (i < litCount):
            strip.setPixelColor(i, onColour)
        else:
            strip.setPixelColor(i, Color(0,0,0))
    if lastLEDIsLit:
        strip.setPixelColor(14, onColour)
    else:
        strip.setPixelColor(14, offColour)
    strip.show()

def showTime(stri5, hour, minute):
    startTimeInMinutes = 6 * 60
    barSizeInMinutes = 10
    currentTime = (hour * 60) + minute
    if (currentTime < startTimeInMinutes):
        lightLEDs(False, 0)
    elif (currentTime > (startTimeInMinutes + (20 * barSizeInMinutes))):
        lightLEDs(False, 0)
    else:
        lightLEDs(True, math.floor((currentTime - startTimeInMinutes) / barSizeInMinutes))


while True:
    now = datetime.now().time()
    showTime(strip, now.hour, now.minute)
    time.sleep(30)

