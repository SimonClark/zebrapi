#!/usr/bin/python

import conf
import utils
import subprocess
import urllib

params = {
        'name' : conf.name,
        'ip' : subprocess.check_output(['hostname',  '-I']).strip()
}
params = urllib.urlencode(params);

print utils.getURL("%s?%s" % (conf.putipURL, params))