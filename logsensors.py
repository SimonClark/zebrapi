#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import math
import sys
import time
import json
import bme280
import smbus2
from urllib.parse import urlencode
import requests

def postToURL(urlStr, params):
	try:
		data = urlencode(params)
		r = requests.post(url = urlStr + '?' + data, data = params) 
  
		return r.text 

# 		data = urllib.urlencode(params)
# 		print(data)
# 		req = urllib2.Request(urlStr, data)
# 		response = urllib2.urlopen(req)
# 		return response.read()
	except IOError:
# 		print IOError
		print('Cannot open URL %s for reading' % urlStr)
		return "postURL Connection Error"

def getSensorData():
	global bus, address, calibration_params
	return bme280.sample(bus, address, calibration_params)


def sensorCycle():
	averages = {"temperature":0.0, "pressure":0.0, "humidity": 0.0, "samplesize":0}
	for counter in range(1, 10):
		data = getSensorData()
		averages["temperature"] = averages["temperature"] + data.temperature
		averages["pressure"] = averages["pressure"] + data.pressure
		averages["humidity"] = averages["humidity"] + data.humidity
		averages["samplesize"] = averages["samplesize"] + 1
		time.sleep(0.2)
	averages["temperature"] = averages["temperature"] / averages["samplesize"]
	averages["pressure"] = averages["pressure"] / averages["samplesize"]
	averages["humidity"] = averages["humidity"] / averages["samplesize"]
	return averages	

postSensorURL = 'http://dev.zebraspot.com/eventtrack/interface/logSensor.php'
port = 1
address = 0x76
bus = smbus2.SMBus(port)
calibration_params = bme280.load_calibration_params(bus, address)

# loop forever
while True:
	# get the data
	sensorData = sensorCycle()
# 	print(sensorData)
	# send the data
	jsonPacket = json.dumps(sensorData)
	# print(sensorData)
	response = postToURL(postSensorURL, {"datapacket": jsonPacket})
	# print(response)
	time.sleep(300)

