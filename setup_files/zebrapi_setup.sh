sudo apt-get install ow-shell python-ow vim supervisor
cp /var/zebrapi/supervisor.conf/sensors.conf /etc/supervisor/conf.d/
cp /var/zebrapi/supervisor.conf/wakeup.conf /etc/supervisor/conf.d/
supervisorctl restart all
echo "alias ll=\"ls -lah\"" >> ~/.bash_aliases