# README #

adding the following line at the end of /etc/ssh/sshd_config
```
IPQoS cs0 cs0
```

Install dependencies:
```
sudo raspi-config // enable i2c bus
sudo apt-get install ow-shell python-ow vim supervisor
sudo apt-get install i2c-tools python-pip
sudo pip3 install smbus2
sudo pip3 install RPi.bme280

```

Create a ssh key for this pi.
```
ssh-keygen
cat ~/.ssh/id_rsa.pub
```
copy the output to the bitbucket account's ssh keys

Checkout the zebrapi code.
```
cd ~
git clone git@bitbucket.org:SimonClark/zebrapi.git
sudo mv zebrapi /var/
cd /var/zebrapi/
```

install the supervisor control scripts:
```
cp /var/zebrapi/supervisor.conf/sensors.conf /etc/supervisor/conf.d/
cp /var/zebrapi/supervisor.conf/wakeup.conf /etc/supervisor/conf.d/
```
Restart all supervisor scripts:
```
supervisorctl restart all
```

Create and edit the zebraPi configuration to update the machine name:
```
cd /var/zebrapi
cp conf.default.py conf.py
vim conf.py
```

Install the IP registration script:
```
sudo crontab -e
```
 and add the line:
```
@reboot /var/zebrapi/putip.sh
```
        
