#!/usr/bin/python

name = "wakeup"
pyFolder = "/var/zebrapi/"
platform = "pi"
logFile = "/var/log/zebrapi/logfile.txt"
debug = False

putipURL = 'http://dev.zebraspot.com/ips/interface/putip.php'
logLevel = 0

ledStripPin = 12
