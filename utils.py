#!/usr/bin/python

import shutil
import time
import conf
import math
import os.path
import urllib
import urllib2
import json

try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")

def initGPIO():
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(conf.doorLockPin, GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(conf.outsideLedPin, GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(conf.onboardLedPin, GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(conf.powerSwitchPin, GPIO.IN)
	GPIO.setup(conf.syncButtonPin, GPIO.IN)

def copyFile(srcFile, destFile):
	shutil.copyfile(srcFile, destFile)
	


def log(msg, level = 3):
	global logs
	if level <= conf.logLevel:
		print msg
		#h = open(os.path.join(logDirectory,now+".log"),'a')
		h = open(os.path.join(logDirectory,conf.logFile),'a')
		h.write(str(msg)+"\n")
		h.close()
	

def terminate():
	exit()
		


def saveToFile(file, contents):
	h = open(file,'w')
	h.write(str(contents))
	h.close()


def getURL(urlStr):
	if conf.debug:
		print urlStr
	try:
		fileHandle = urllib2.urlopen(urlStr,timeout=3)
		str1 = fileHandle.read()
		fileHandle.close()
		return str1
	except IOError:
		print 'Cannot open URL %s for reading' % urlStr
		return False



def postURL(urlStr, params):
	try:
		data = urllib.urlencode(params)
		req = urllib2.Request(urlStr, data)
		response = urllib2.urlopen(req)
		return response.read()
	except IOError:
# 		print IOError
		print 'Cannot open URL %s for reading' % urlStr
		return "postURL Connection Error"


def writeToFile(filePath, contents):
	h = open(filePath,'w')
	h.write(contents)
	h.close()

def readFile(filePath):
	if os.path.isfile(filePath):
		h = open(filePath,'r')
		contents = h.read()
		h.close()
		return contents		
	return ''
